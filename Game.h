#ifndef GAME_H
#define GAME_H

#include <irrlicht.h>
#include "GameGUI.h"
#include "MyEventReceiver.h"
#include "Joueur.h"
#include <fstream>
#include <vector>
#include <Windows.h>
#include <string>



using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

class Game
{
    public:
        Game();
        virtual ~Game();

        void run();

        GameGUI *gui;
    protected:

    private:
        IrrlichtDevice *device;
        IVideoDriver *driver;
        IGUIEnvironment *guienv;

        MyEventReceiver *receiver;


};

#endif // GAME_H
