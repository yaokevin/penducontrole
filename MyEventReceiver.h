#ifndef MYEVENTRECEIVER_H
#define MYEVENTRECEIVER_H
#include <iostream>
#include <irrlicht.h>
#include "enums.h"



using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

class Game;

class MyEventReceiver : public IEventReceiver
{
    public:
        MyEventReceiver(Game *Pendu);
        virtual ~MyEventReceiver();

        virtual bool OnEvent(const SEvent& event);
    protected:

    private:
        Game *Pendu;
        std::string memoire;
};

#endif // MYEVENTRECEIVER_H
