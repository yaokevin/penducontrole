#ifndef GAMEGUI_H
#define GAMEGUI_H

#include <irrlicht.h>
#include "enums.h"
#include <string>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

class GameGUI
{
    public:
        GameGUI(IGUIEnvironment *guienv);
        virtual ~GameGUI();
        IGUIStaticText *fenetreDebut;
        IGUIStaticText *fenetreJeu;

        //Fenetre D�but
        IGUIButton *btnDebutPartie;
        IGUIButton *btnQuitter;
        IGUIButton *btnOk;

        //Fen�tre Jeu
        IGUIStaticText *ChaineADeviner;
        irr::gui::IGUIEditBox *EntrerCaractere;
        IGUIStaticText *NbChances;
        IGUIButton *btnRecommencer;

        std::string MotAtrouver[4000];

    protected:

    private:
        IGUIEnvironment *guienv;




};

#endif // GAMEGUI_H
