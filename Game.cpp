#include "Game.h"

Game::Game()
{
    //ctor
    //Cr�er le device irrlicht
    device = createDevice(EDT_SOFTWARE, dimension2d<u32>(640, 480), 16, false, false, false, 0);
    driver = device->getVideoDriver();
    guienv = device->getGUIEnvironment();

    //Cr�ation de la classe GUI
    gui = new GameGUI(guienv);

    //Event receiver
    receiver = new MyEventReceiver(this);

    //Lien entre le Device et l'event reciever
    device->setEventReceiver(receiver);

    std::string MotAtrouver[100];

    std::ifstream fs("Mots.txt"); //Connection au fichier text

    for(int i = 0;i < 100; i++)
    {
        fs >> MotAtrouver[i];
        getline(fs, MotAtrouver[i]);
        std::cout << MotAtrouver[i] << std::endl;
    }
     if(!fs)
    {
        std::cout << "Impossible d'ouvrir le fichier" << std::endl;
    }
    fs.close();
}

Game::~Game()
{
    //dtor
    device->drop();
}

void Game::run() {
    //Boucle de rendu principale
    while (device->run())
    {
        driver->beginScene(true, true, SColor(0,200,200,200));

        guienv->drawAll();

        driver->endScene();
    }
}
