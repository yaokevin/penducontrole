#include "GameGUI.h"

GameGUI::GameGUI(IGUIEnvironment *guienv)
{
    //ctor
    this->guienv = guienv;

    fenetreDebut = guienv->addStaticText(L"", rect<int>(0,0,640,480));

    btnDebutPartie = guienv->addButton(rect<int>(10,70,120+100,70+20), fenetreDebut, GUI_ID_DEBUTPARTIE, L"Demarrer la partie");
    btnQuitter = guienv->addButton(rect<int>(10,100,120+100,100+20), fenetreDebut, GUI_ID_QUITTER, L"Quitter");

    fenetreJeu = guienv->addStaticText(L"", rect<int>(0,0,640,480));
    fenetreJeu->setVisible(false);

    ChaineADeviner = guienv->addStaticText(L"Mot a deviner", rect<int>(90,20,250,90+20), false, false, fenetreJeu);
    NbChances = guienv->addStaticText(L"Chances :", rect<int>(250,20,200+100,100+20), false, false, fenetreJeu);
    NbChances = guienv->addStaticText(L"7", rect<int>(265,40,200+100,100+20), false, false, fenetreJeu);
    EntrerCaractere = guienv->addEditBox(L"", rect<int>(10,40,220,40+20), true, fenetreJeu);
    btnOk = guienv->addButton(rect<int>(10,70,120+100,70+20), fenetreJeu, GUI_ID_OK, L"Ok");
    btnRecommencer = guienv->addButton(rect<int>(10,100,120+100,100+20), fenetreJeu, GUI_ID_RECOMMENCER, L"Recommencer");
}

GameGUI::~GameGUI()
{
    //dtor
}
