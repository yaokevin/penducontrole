#include "Game.h"

int main()
{
    //Classe principale du jeu
	Game *Pendu = new Game();

	//Boucle principale
    Pendu->run();

    delete Pendu;

    return 0;
}

